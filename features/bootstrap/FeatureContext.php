<?php

require __DIR__ . "/../../vendor/autoload.php";

use Behat\Behat\Context\Context;
use LaSalle\Recuperacion\Module\Quote\Infrastructure\ApiQuoteRepository;
use LaSalle\Recuperacion\Module\Quote\Domain\RamdomQuoteSearch;

/**
 * Defines application features from the specific context.
 */
class FeatureContext implements Context
{
    const CORRECT_EXECUTION = 0;

    private $quoteRepository;
    private $randomQuote;
    private $projectName;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
        $this->quoteRepository = new ApiQuoteRepository();
        $this->randomQuote = new RamdomQuoteSearch($this->quoteRepository);
    }

    /**
     * @Given /^I have a "([^"]*)" project$/
     */
    public function iHaveAProject($projectName)
    {
        $this->projectName = $projectName;
        if (!file_exists($this->projectName)) {
            throw new \Exception(sprintf("We don't have a project with %s name", $this->projectName));
        }
    }

    /**
     * @When /^I call the quote project$/
     */
    public function iCallTheQuoteProject()
    {
        exec("php " . $this->projectName, $output, $status);

        if ($status !== self::CORRECT_EXECUTION) {
            throw new \Exception("Quote project has an error.");
        }
    }

    /**
     * @Then /^we must get a quote$/
     */
    public function weMustGetAQuote()
    {
        if (!in_array($this->randomQuote->__invoke(), $this->quoteRepository->all())) {
            throw new \Exception(sprintf("We don't get a quote"));
        }
    }



}