Feature: Get a quote
  In order to have a quote from a repository
  As a user
  I want to get the quote

  Scenario: Get a quote
    Given I have a "quote.php" project
    When I call the quote project
    Then we must get a quote