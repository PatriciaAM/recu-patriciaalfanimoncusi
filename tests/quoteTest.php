<?php

namespace LaSalle\RecuperacionTest;

use PHPUnit\Framework\TestCase;

final class quoteTest extends TestCase
{
    /** @test */
    public function it_should_not_be_null()
    {
        $output = exec('php quote.php');
        $this->assertNotNull(isset($output));
    }

    /** @test */
    public function it_should_return_a_valid_string()
    {
        $output = exec('php quote.php');
        $this->assertTrue(is_string($output));
    }
}