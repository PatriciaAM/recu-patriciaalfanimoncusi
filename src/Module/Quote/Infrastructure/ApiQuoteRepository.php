<?php

namespace LaSalle\Recuperacion\Module\Quote\Infrastructure;

use LaSalle\Recuperacion\Module\Quote\Domain\Quote;
use LaSalle\Recuperacion\Module\Quote\Domain\QuoteRepository;

final class ApiQuoteRepository  implements QuoteRepository
{
    CONST QUOTES_MAXIMUM = 6;

    private $quotes = [];

    public function all(): array
    {
        $this->getStringQuoteApi();
        return $this->createQuoteArray();
    }

    private function getStringQuoteApi()
    {
        for ($i = 0; $i < self::QUOTES_MAXIMUM; $i++) {
            $this->consumeApiTronalDump();
        }
    }

    private function consumeApiTronalDump()
    {
        $url = 'https://api.tronalddump.io/random/quote';
        $data = @file_get_contents($url);
        $jsonapi_string = json_decode($data);
        array_push($this->quotes, $jsonapi_string->{'value'});
    }

    /**
     * @return array
     */
    public function createQuoteArray(): array
    {
        return array_map(array(Quote::class, 'fromString'), $this->quotes);
    }
}