<?php

namespace LaSalle\Recuperacion\Module\Quote\Domain;

use LaSalle\Recuperacion\Module\Quote\Domain\Exception\EmptyQuotesArrayException;

final class RamdomQuoteSearch
{
    private $repository;

    public function __construct(QuoteRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(): string
    {
        $quotes = $this->repository->all();
        $this->checkQuotesArrayIsNotEmpty($quotes);
        return $this->getRandomQuotes($quotes);
    }

    private function getRandomQuotes(array $quotes): Quote
    {
        return $quotes[mt_rand(0, count($quotes) - 1)];
    }

    /**
     * @param $quotes
     * @throws EmptyQuotesArrayException
     */
    public function checkQuotesArrayIsNotEmpty($quotes)
    {
        if (empty($quotes)) {
            throw new EmptyQuotesArrayException('Quote can not empty');
        }
    }

}