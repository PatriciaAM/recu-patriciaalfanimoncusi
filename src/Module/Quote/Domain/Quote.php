<?php

namespace LaSalle\Recuperacion\Module\Quote\Domain;

class Quote
{
    private $quote;

    private function __construct(string $quote)
    {
        $this->quote = $quote;
    }

    public static function fromString(string $string): self
    {
        return new self($string);
    }

    public function __toString()
    {
        return (string)$this->quote;
    }
}