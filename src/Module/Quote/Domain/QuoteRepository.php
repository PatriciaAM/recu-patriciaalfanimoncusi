<?php

namespace LaSalle\Recuperacion\Module\Quote\Domain;

interface QuoteRepository
{
    public function all(): array;
}