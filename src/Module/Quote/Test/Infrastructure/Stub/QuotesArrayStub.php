<?php

namespace LaSalle\Recuperacion\Module\Quote\Test\Infrastructure\Stub;


final class QuotesArrayStub
{
    public static function create(...$quotes): array
    {
        return $quotes;
    }
}