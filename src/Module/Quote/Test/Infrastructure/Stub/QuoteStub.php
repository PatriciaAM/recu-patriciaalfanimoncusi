<?php

namespace LaSalle\Recuperacion\Module\Quote\Test\Infrastructure\Stub;

use Faker\Factory;
use LaSalle\Recuperacion\Module\Quote\Domain\Quote;

final class QuoteStub
{
    public static function create(string $quote): Quote
    {
        return Quote::fromString($quote);
    }

    public static function random(): Quote
    {
        return self::create(Factory::create()->sentence);
    }
}