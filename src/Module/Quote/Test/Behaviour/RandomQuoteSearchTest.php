<?php

namespace LaSalle\Recuperacion\Module\Quote\Test\Behaviour;

use LaSalle\Recuperacion\Module\Quote\Domain\Exception\EmptyQuotesArrayException;
use LaSalle\Recuperacion\Module\Quote\Domain\QuoteRepository;
use LaSalle\Recuperacion\Module\Quote\Domain\RamdomQuoteSearch;
use LaSalle\Recuperacion\Module\Quote\Test\Infrastructure\Stub\QuotesArrayStub;
use LaSalle\Recuperacion\Module\Quote\Test\Infrastructure\Stub\QuoteStub;
use LaSalle\RecuperacionTest\TestCase\UnitTestCase;
use Mockery\MockInterface;

final class RandomQuoteSearchTest extends UnitTestCase
{
    /** @var MockInterface|QuoteRepository */
    private $quoteRepository;

    /** @var RamdomQuoteSearch */
    private $quoteSearcher;

    public function setUp()
    {
        parent::setUp();

        $this->quoteRepository = $this->mock(QuoteRepository::class);
        $this->quoteSearcher   = new RamdomQuoteSearch($this->quoteRepository);
    }


    /** @test */
    public function it_should_return_a_random_string()
    {
        $quote       = QuoteStub::random();
        $quotesArray = QuotesArrayStub::create($quote);

        $this->should_return_quotes_array($quotesArray);

        $this->assertEquals(
            $quote,
            $this->quoteSearcher->__invoke()
        );
    }

    private function should_return_quotes_array(array $quotesArray)
    {
        $this->quoteRepository
            ->shouldReceive('all')
            ->once()
            ->andReturn($quotesArray);
    }
    /** @test */
    public function it_should_return_exception_when_quotes_is_empty()
    {
        $this->expectException(EmptyQuotesArrayException::class);

        $quotesArrayIsEmpty = [];
        $this->should_return_quotes_array($quotesArrayIsEmpty);

        $this->quoteSearcher->__invoke();
    }

}