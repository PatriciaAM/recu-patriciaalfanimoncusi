<?php

namespace LaSalle\Recuperacion\Module\Quote\Test\Infrastructure;


use LaSalle\Recuperacion\Module\Quote\Infrastructure\ApiQuoteRepository;

use PHPUnit\Framework\TestCase;

final class ApiQuoteRepositoryTest extends TestCase
{
    private $repository;

    /** @test */
    public function it_should_return_array_of_quotes_when_calling_all_method()
    {
        $this->assertNotNull(
            $this->repository()->all()
        );
    }

    private function repository(): ApiQuoteRepository
    {
        return $this->repository = $this->repository ?: new ApiQuoteRepository();
    }
}